import React from 'react';
import { navigate } from 'gatsby';
import { Container, Row, Col } from 'react-bootstrap';
import { FloatMenu, BackButton, Seo } from './../components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/eficiencia-termica.css';

export default () => {

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <FloatMenu />
            <Row className="justify-content-md-center">
                <Col md={8} lg={8} className="left2"></Col>
                <Col md={4} lg={4}>
                    <Row className="justify-content-md-center">
                        <label className="title">Eficiencia <label className="titleBold">térmica</label></label>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10} >
                            <h1 className="title2">VENTANAS DE PVC CON VIDRIOS TERMOPANEL</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description">Aislación térmica y acústica.</p>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <h1 className="title2">CALEFACCIÓN POR RADIADORES A GAS</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description">Limpia, programable y de alta eficiencia calórica.</p>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <h1 className="title2">AISLACIÓN TÉRMICA ENVOLVENTE 360</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description">Capa aislante que mantiene una temperatura optima y confortable al interior de la casa.</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );

}