import React from "react";
import { navigate } from 'gatsby';
import { Container, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/modelosCasas.css'
import { ContainerTitle, ContainerSubTitle, ModelosCasas, FloatMenu, BackButton, Seo } from '../components';

export default () => {

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={() => navigate('/modelos-metros')} />
            <FloatMenu />
            <ContainerTitle md={4} lg={4} title="Modelos"
                subTitle={<p>Elige en parque los avellanos</p>} />
            <ContainerSubTitle text="Tiplogía casas" />
            <Row className="justify-content-md-center" style={{ marginTop: '5%' }}>
                <ModelosCasas img={require('./../images/modelo1.jpg')} onClick={() => navigate('/planta-modelo1') } modelo="Modelo 97 M" />
                <ModelosCasas img={require('./../images/modelo2.jpg')} onClick={() => navigate('/planta-modelo2') } modelo="Modelo 114 M" />
                <ModelosCasas img={require('./../images/modelo3.jpg')} onClick={() => navigate('/planta-modelo3') } modelo="Modelo 115 M" />
            </Row>
        </Container>
    );

}