import React, { useState } from 'react';
import { navigate } from 'gatsby';
import { Container, Row, Col } from 'react-bootstrap';
import { FloatMenu, BackButton, Seo, Bullet } from './../components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/cocinas-360.css';

export default () => {

    const [info, setInfo] = useState({
        title: 'Comedor de diario integrado',
        text: 'Un espacio perfecto para comenzar tu día compartiendo y disfrutando.',
        colorB1: '#443579',
        colorB2: '',
        colorB3: '',
        scale: ''
    })

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <FloatMenu />
            <Row className="justify-content-md-center">
                <Col md={8} lg={8} className="left2-cocinas360">
                    <Bullet 
                        top={61} 
                        left={13}
                        color={info.colorB1} 
                        onClick={ () => 
                            setInfo({...info, 
                                title: 'Comedor de diario integrado', 
                                text: 'Un espacio perfecto para comenzar tu día compartiendo y disfrutando.',
                                colorB1: '#443579',
                                colorB2: '',
                                colorB3: ''
                            }) 
                        } 
                    />
                    <Bullet 
                        top={45} 
                        left={23} 
                        color={info.colorB2}
                        onClick={ () => 
                            setInfo({...info, 
                                title: 'Lorem Ipsum 2', 
                                text: "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                                colorB1: '',
                                colorB2: '#443579',
                                colorB3: ''
                            }) 
                        } 
                    />
                    <Bullet 
                        top={58} 
                        left={33} 
                        color={info.colorB3}
                        onClick={ () => 
                            setInfo({...info, 
                                title: 'Lorem Ipsum 3', 
                                text: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                                colorB1: '',
                                colorB2: '',
                                colorB3: '#443579'
                            }) 
                        } 
                    />
                </Col>
                <Col md={4} lg={4}>
                    <Row className="justify-content-md-end">
                        <Col md={11} lg={11} className="containerTitleCocina">
                            <Row className="justify-content-md-end mr-5">
                                <h1 className="titleCocina360">Cocinas 360°</h1>
                            </Row>
                            <Row className="justify-content-md-end mr-5">
                                <h1 className="subTitleCocina">Un espacio que lo tiene todo</h1>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-end mr-5 mt-5">
                        <Col md={10} lg={10} >
                            <h1 className="title2">{info.title}</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-end mr-5">
                        <Col md={10} lg={10}>
                            <p className="description">{info.text}</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );

}