import React from 'react';
import { navigate } from 'gatsby';
import { Container, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ContainerTitle, ContainerSubTitle, Models, BackButton, Seo } from './../components';

export default () => {

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <ContainerTitle md={4} lg={4} title="Modelos" subTitle ="Elige en parque los avellanos" />
            <ContainerSubTitle text="Tipología departamentos" />
            <Row className="justify-content-md-center" style={{marginTop: '10%'}}>
                <Models text="2D + 1B" onPress={ () => navigate('/modelosMetros')} />
                <Models text="2D + 2B" />
                <Models text="3D + 2B" />
            </Row>
        </Container>
    );

}