import React from 'react';
import { navigate} from 'gatsby';
import { Container, Row, Col } from 'react-bootstrap';
import { FloatMenu, BackButton, Seo } from './../components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/optimizacion-maxima.css';

export default () => {

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <FloatMenu />
            <Row className="justify-content-md-center">
                <Col md={8} lg={8} className="left3"></Col>
                <Col md={4} lg={4}>
                    <Row className="justify-content-md-center">
                        <label className="title">Optimización <label className="titleBold">máxima</label></label>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <h1 className="title2-3">DORMITORIO PRINCIPAL EN SUITE, TU ELIGES</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description2">¿En primer o segundo piso? Puedes optar por la privacidad y comodidad de tener tu dormitorio en primer piso o la tranquilidad de estar cerca a los más pequeños.</p>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <h1 className="title2-3">SALA DE ESTAR EN PRIMER O SEGUNDO PISO</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description2">2 modelos de casas que integran este espacio dinámico para que lo vivas como tu familia quiera.</p>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <h1 className="title2-3">DISFRUTA AL AIRE LIBRE</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description2">Vive las terrazas como un espacio protagonista para compartir y disfrutar desde el primer día.</p>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <h1 className="title2-3">ESTACIONAMIENTOS CON PAVIMENTO COMPLETO</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description2">Un detalle exterior que aporta funcionalidad y estética.</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );

}