import React, { useState } from 'react';
import { navigate } from 'gatsby';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { FloatMenu, BackButton, Button, ButtonModel, Carousel, Seo } from './../components';
import dataEntorno from './../data/dataEntorno';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/ubicacion-servicios.css';

export default () => {

    const [map, setMap] = useState(require('./../images/map/map-market.png'));
    const [color, setColor] = useState({
        market: '#443579',
        college: '',
        servicentro: ''
    })
    const [option, setOption] = useState('mapa');

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <FloatMenu />
            {
                option === 'mapa' &&
                <Row className="justify-content-md-around" style={{minHeight: '87.1vh'}}>
                    <Col md={4} lg={4} >
                        <Row className="justify-content-md-end">
                            <Col md={9} lg={9} className="containerTitleCocina">
                                <Row className="justify-content-md-start ml-5">
                                    <h1 className="titleUbicacion">Ubicación</h1>
                                </Row>
                                <Row className="justify-content-md-start ml-5">
                                    <h1 className="titleBoldUbicacion">Y Servicios</h1>
                                </Row>
                            </Col>
                        </Row>
                        <Row className="justify-content-md-end ml-5">
                            <Col md={9} lg={9}>
                                <p className="descriptionUbicacion">Vive en un barrio privilegiado de Los Ángeles, acá tendrás al alcance todos los serivicios que necesitas.</p>
                            </Col>
                        </Row>
                        <Row className="justify-content-md-end ml-5">
                            <Col md={9} lg={9} className="containerServicios">
                                <ButtonModel 
                                    text="Supermercado" 
                                    color={color.market} 
                                    onClick={ () => {
                                        setMap(require('./../images/map/map-market.png'));
                                        setColor({
                                            ...color,
                                            market: '#443579',
                                            servicentro: '',
                                            college: ''
                                        })
                                    }}/>
                                <ButtonModel 
                                    text="Servicentros" 
                                    color={color.servicentro} 
                                    onClick={ () => {
                                        setMap(require('./../images/map/map-servicentro.png'));
                                        setColor({
                                            ...color,
                                            market: '',
                                            servicentro: '#443579',
                                            college: ''
                                        })
                                    }}/>
                                <ButtonModel 
                                    text="Colegios" 
                                    color={color.college} 
                                    onClick={ () => {
                                        setMap(require('./../images/map/map-college.png'));
                                        setColor({
                                            ...color,
                                            market: '',
                                            servicentro: '',
                                            college: '#443579'
                                        })
                                    }}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={7} lg={7} 
                        style={{
                            backgroundImage: `url(${map})`,
                            backgroundSize: 'cover',
                            backgroundRepeat: 'no-repeat',
                            height: '80vh'
                        }}
                    ></Col>
                </Row>
            }
            {
                option === 'entorno' && 
                    <Row style={{minHeight: '80vh'}}>
                        <Carousel imgs={dataEntorno} />
                    </Row>
            }
            {
                option === 'servicios' && 
                <>
                <Row style={{minHeight: '87.1vh'}}>
                    <Col md={10} lg={10} className="offset-md-1">
                        <Row className="justify-content-md-center">
                            <Col md={7} lg={7} className="containerServices"></Col>                   
                        </Row>
                        <Row className="justify-content-md-around mt-5">
                            <Col md={1} lg={1} >
                                <Image src={require('./../images/servicios/Iconos-05.png')} className="imgServicios" fluid/>                        
                            </Col>
                            <Col md={1} lg={1} >
                                <Image src={require('./../images/servicios/Iconos-04.png')} className="imgServicios" fluid/>
                            </Col>
                            <Col md={1} lg={1} >
                                <Image src={require('./../images/servicios/Iconos-01.png')} className="imgServicios" fluid/>
                            </Col>
                            <Col md={1} lg={1} >
                                <Image src={require('./../images/servicios/Iconos-02.png')} className="imgServicios" fluid/>
                            </Col>
                            <Col md={1} lg={1} >
                                <Image src={require('./../images/servicios/Iconos-03.png')} className="imgServicios" fluid/>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </>
            }
            
            <Row>
                <div className="container-buttons">
                    <Button text="Ver Mapa" onClick={ () => setOption('mapa')} />
                    <Button text="Entorno" onClick={ () => setOption('entorno')} />
                    <Button text="Servicios" onClick={ () => setOption('servicios')} />
                </div>
            </Row>
        </Container>
    );

}