import React from "react";
import { navigate } from 'gatsby';
import { Container, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/portada.css'
import { ContainerTitle, Card, FloatMenu, BackButton, Seo } from './../components';
import { motion } from "framer-motion";

export default () => {

	return (
        
		<Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/')} />
            <FloatMenu />

            <motion.div
                    //className="block"
                    // onClick={() => setIsActive(!isActive)}
                    animate={{
                        scale: [0.99, 1,0.99],
                    }}
                    transition={{ delay:0.3 }}
                >
                    <ContainerTitle md={6} lg={6} title="Parque Los Avellanos" subTitle ="Diseño 360, le dimos un giro a cada espacio" />
                </motion.div>

			
			<Row className="justify-content-md-center mt-5">
                <Card 
                    onClick={ () => navigate('/ubicacion-entorno') }
                    md={3}
                    lg={3}
                    title={<><label>Ubicación y</label> <label id="title2">Entorno</label></>}
                    subTitle="Un barrio residencial por excelencia, rodeado de servicios y full conectado. Al interior del Condominio hemos incorporado urbanización eléctrica subterránea, aportando estética y limpieza visual al entorno."
                    img={require('./../images/pilares1.png')}
                />
                <Card 
                    onClick={ () => navigate('/eficiencia-termica') }
                    md={3}
                    lg={3}
                    title={<><label>Eficiencia</label> <label id="title2">Térmica</label></>}
                    subTitle="Integramos el confort térmico de la casa por medio de terminaciones y tecnología que permiten mantener por mayor tiempo una temperatura ideal tanto en invierno como en verano."
                    img={require('./../images/pilares2.jpg')}
                />
                <Card 
                    onClick={ () => navigate('/optimizacion-maxima') }
                    md={3}
                    lg={3}
                    title={<><label>Optimización</label> <label id="title2">Máxima</label></>}
                    subTitle="Descubre la excelente distribución al interior y exterior junto a un equipamiento que se pensó como parte del diseño, para que puedas aprovechar cada sector al máximo."
                    img={require('./../images/pilares2-1.jpg')}
                />
			</Row>
		</Container>
	);

}