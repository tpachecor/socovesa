import React, { useState } from 'react';
import { navigate } from 'gatsby';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { FloatMenu, BackButton, Seo, Bullet, Modal } from './../components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/master-plan.css';

export default () => {

    const [modal, setModal] = useState({
        open: false,
        top: '',
        left: '', 
        text: ''
    });

    return (
        <Container fluid>
            <FloatMenu />
            <BackButton onClick={ () => navigate('/pilares') } />
            <Seo />
            <Row className="justify-content-md-center">
                <Col md={6} lg={6} className="containerTitleMasterPlan mt-5 pt-3 pb-3 pl-1 pr-1">
                    <label style={{ textAlign: 'center', display: 'block', textTransform: 'uppercase', fontFamily: 'Bold', fontSize: '40px'}}>Master Plan <label style={{textTransform: 'uppercase', fontFamily: 'Light', fontSize: '40px'}}> Los avellanos</label></label>
                </Col>
                <Col md={1} lg={1} className="containerCursor mt-5" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <Image src={require('./../images/cursor.png')} fluid />
                </Col>
            </Row>
            <Row className="justify-content-md-center mt-5">
                <Col lg={10}>
                    <Bullet top={50} left={30} onClick={ () => setModal({...modal, open: true, top: 50, left: 30, text: '1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.' }) } />
                    <Bullet top={60} left={40} onClick={ () => setModal({...modal, open: true, top: 60, left: 40, text: '2. Lorem Ipsum is simply dummy text of the printing and typesetting industry.' }) }/>
                    <Bullet top={70} left={60} onClick={ () => setModal({...modal, open: true, top: 70, left: 60, text: '3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.' }) }/>
                    <Bullet top={40} left={50} onClick={ () => setModal({...modal, open: true, top: 40, left: 50, text: '4. Lorem Ipsum is simply dummy text of the printing and typesetting industry.' }) }/>
                    {
                        modal.open && 
                            <Modal top={modal.top} left={modal.left} text={modal.text} img={require('./../images/modelo2/modelo2-2.jpg')} />
                    }
                    <Image src={require('./../images/masterplan.png')} fluid style={{ marginLeft: '15%', width: '70%', height: '70vh' }} onClick={ () => setModal({...modal, open: false}) }/>
                </Col>
            </Row>
        </Container>
    );

}