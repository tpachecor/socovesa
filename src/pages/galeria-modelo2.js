import React from 'react';
import { navigate } from 'gatsby';
import { Carousel, Button, FloatMenu, BackButton, Seo } from './../components';
import dataModelo from './../data/galeriaModelo2'

export default () => {

    return (
        <>
            <Seo />
            <BackButton onClick={ () => navigate('/modelos-casas') } />
            <FloatMenu />
            <Carousel imgs={dataModelo}/>
            <div className="container-buttons">
                <Button text="Planta" onClick={ () => navigate('/planta-modelo2') } />
                <Button text="Ubicación" />
                <Button text="Galería" onClick={ () => navigate('/galeria-modelo2') } />
                <Button text="Video" />
			</div>
        </>
    );

}