import React from "react";
import { navigate } from 'gatsby';
import { Container, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/modelosCasas.css'
import { ContainerTitle, ContainerSubTitle, Models, FloatMenu, BackButton, Seo } from '../components';

export default () => {

	return (
		<Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <FloatMenu />
            <ContainerTitle md={6} lg={6} title="Modelos" 
            subTitle ={<label>LA PROPUESTA ES UNA SOLA, UN CONDOMINIO QUE POR DENTRO Y POR FUERA LO TIENE TODO. <label style={{fontWeight: 'bold', color: '#000'}}>ESO ES PENSAR 360.</label></label>} />
            <ContainerSubTitle text="Tiplogía casas" />
                <Row className="justify-content-md-center" style={{marginTop: '5%'}}>
                    <Models text="97" plain onPress={ () => navigate('/modelos-casas') } />
                    <Models text="114" plain onPress={ () => navigate('/modelos-casas') } />
                    <Models text="115" plain onPress={ () => navigate('/modelos-casas') } />
                </Row>
		</Container>
	);

}