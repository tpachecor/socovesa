import React from 'react';
import { navigate } from 'gatsby';
import { Container, Row, Col } from 'react-bootstrap';
import { FloatMenu, BackButton, Seo } from './../components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/ubicacion-entorno.css';

export default () => {

    return (
        <Container fluid>
            <Seo />
            <BackButton onClick={ () => navigate('/pilares')} />
            <FloatMenu />
            <Row className="justify-content-md-center">
                <Col md={8} lg={8} className="left"></Col>
                <Col md={4} lg={4}>
                    <Row className="justify-content-md-center">
                        <label className="title">Ubicación y <label className="titleBold">entorno</label></label>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10} >
                            <h1 className="title2-1" >conectate con la naturaleza</h1>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <p className="description">Además de áreas verdes que integran un paisajismo que se fusiona con el entorno natural
                            creando  espacios  para  que también disfrutes al exterior de tu casa, y visas  la  experiencia  de  una  vida  en
                        condominio 360. </p>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md={10} lg={10}>
                            <li className="points"> <span>Piscina</span> </li>
                            <li className="points"> <span>Plazas interiores con juegos infantiles</span> </li>
                            <li className="points"> <span>Senderos</span> </li>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );

}