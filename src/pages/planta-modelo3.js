import React, { useState } from 'react';
import { navigate } from 'gatsby';
import { Row, Col, Image } from 'react-bootstrap';
import { ButtonModel, FloatMenu, BackButton, Button, Seo } from '../components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/plantas.css';

export default () => {

    const [piso, setPiso] = useState(require('./../images/plantas/115P1.png'));
    const [color, setColor] = useState({
        primerPiso: '#443579',
        segundoPiso: ''
    });

    return (
        <>
            <Seo />
            <BackButton onClick={ () => navigate('/modelos-casas') } />
            <FloatMenu />
            <Row className="justify-content-md-around" style={{minHeight: '87.1vh'}}>
                <Col md={5} lg={5} className="mt-5 offset-md-1" >
                <Row >
                        <Col md={6} lg={6} className="containerTitlePlanta">                    
                            <h1 className="titlePlanta">Modelo 115 M</h1>
                        </Col>
                    </Row>
                    <Row className="pt-5 pl-5">
                        <p className="subTitlePlanta">Especificaciones Técnicas</p>
                    </Row>
                    <Row className="pl-3 ml-4 mt-5 containerEspecificaciones">
                        <Col md={12} lg={12}>
                            <Row>
                                <Col md={4} lg={4}>
                                    <p className="titleEspecificacion">Baños</p>
                                </Col>
                                <Col md={8} lg={8}>
                                    <p className="especificacionPlanta">2 baños, baño de visita en primer piso Baño principal con luz natural, shower door, secundario con tina.</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4}>
                                    <p className="titleEspecificacion">Dormitorios</p>
                                </Col>
                                <Col md={8} lg={8}>
                                    <p className="especificacionPlanta">3 dorms - Principal en primer piso.</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4}>
                                    <p className="titleEspecificacion">Living-comedor-cocina</p>
                                </Col>
                                <Col md={8} lg={8}>
                                    <p className="especificacionPlanta">Cocina aislada de Living-comedor.</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4}>
                                    <p className="titleEspecificacion">Cocina</p>
                                </Col>
                                <Col md={8} lg={8}>
                                    <p className="especificacionPlanta">Amplia Cocina amoblada y equipada. Cubierta de granito. Comedor de diario. Logia separada de la cocina.</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4}>
                                    <p className="titleEspecificacion">Terraza</p>
                                </Col>
                                <Col md={8} lg={8}>
                                    <p className="especificacionPlanta">Terraza pavimentada, salida a terraza por comedor y dormitorio principal. Patio de servicios cerrado.</p>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col md={5} lg={5} className="mt-5" >
                    <Row className="justify-content-md-center">
                        <Col md={6} lg={6} >
                            <ButtonModel text="Primer Piso" onClick={ () => {setPiso(require('./../images/plantas/115P1.png')); setColor({...color, primerPiso: '#443579', segundoPiso: ''}) }} color={color.primerPiso }/>
                        </Col>
                        <Col md={6} lg={6} >
                            <ButtonModel text="Segundo Piso" onClick={ () => {setPiso(require('./../images/plantas/115P2.png')); setColor({...color, primerPiso: '', segundoPiso: '#443579'}) }} color={color.segundoPiso }/>
                        </Col>
                    </Row>
                    <Row>
                        <Image src={piso} fluid/>
                    </Row>
                </Col>
            </Row>
            <div className="container-buttons">
                <Button text="Planta" onClick={ () => navigate('/galeria-modelo3') } />
                <Button text="Ubicación" />
                <Button text="Galería" onClick={ () => navigate('/galeria-modelo3') } />
                <Button text="Video" />
			</div>
        </>
    );

}