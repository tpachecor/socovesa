import React from "react"
import { Container, Row, Col, Image } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/portada.css'
import { ContainerTitle, Seo } from './../components';
import { navigate } from 'gatsby';
import { AnimateSharedLayout } from "framer-motion"

export default () => {

	return (
		<Container fluid onClick={ () => navigate('/pilares')}>
			<Seo />
			<ContainerTitle md={6} lg={6} title="Parque Los Avellanos" subTitle ="Condominio" />
			<Row className="justify-content-md-center">
				<Col md={12} lg={12} className="containerPortada">
					<Image src={require('./../images/portada.png')} className="containerImage imgPortada" id="f1"/>
					<Image src={require('./../images/portada2.jpg')} className="containerImage imgPortada" id="f2"/>
					<Image src={require('./../images/portada3.jpg')} className="containerImage imgPortada" id="f3"/>
				</Col>
			</Row>
			<Row className="justify-content-md-center">
				<Col md={2} lg={2} className="containerLogo">
					<img className="imgLogo" src={require('./../images/logo.png')} alt=""/>
				</Col>
			</Row>
		</Container>
	);

}