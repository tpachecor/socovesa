const imgs = [
    {
        id: 1,
        img: require('./../images/modelo3/modelo3-1.jpg')
    },
    {
        id: 2,
        img: require('./../images/modelo3/modelo3-2.jpg')
    },
    {
        id: 3,
        img: require('./../images/modelo3/modelo3-3.jpg')
    },
    {
        id: 5,
        img: require('./../images/modelo3/modelo3-5.jpg')
    },
]

export default imgs;