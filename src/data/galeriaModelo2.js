const imgs = [
    {
        id: 2,
        img: require('./../images/modelo2/modelo2-2.jpg')
    },
    {
        id: 3,
        img: require('./../images/modelo2/modelo2-3.jpg')
    },
    {
        id: 4,
        img: require('./../images/modelo2/modelo2-4.jpg')
    },
    {
        id: 5,
        img: require('./../images/modelo2/modelo2-5.jpg')
    },
    {
        id: 6,
        img: require('./../images/modelo2/modelo2-6.jpg')
    },
    {
        id: 7,
        img: require('./../images/modelo2/modelo2-7.jpg')
    },
    {
        id: 8,
        img: require('./../images/modelo2/modelo2-8.jpg')
    }
]

export default imgs;