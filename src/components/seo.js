import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';

export default () => {

    const data = useStaticQuery(
        graphql`
            query {
                site {
                    siteMetadata {
                        title,
                        description,
                        author
                    }
                }
            }
        `
    )

    return (
        <Helmet 
            title={data.site.siteMetadata.title}
            htmlAttributes={{
                lang: 'es'
            }}
            meta={[
                {
                    name: 'description',
                    content: data.site.siteMetadata.description
                },
                {
                    name: 'og:title',
                    content: data.site.siteMetadata.title
                },
                {
                    name: 'og:description',
                    content: data.site.siteMetadata.description
                },
                {
                    name: 'og:type',
                    content: 'website'
                }
            ]}
        />
    );

}