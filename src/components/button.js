import React from 'react';
import './styles/button.css';

export default ({text, onClick}) => {

    return (
        <button className="btns" onClick={ () => onClick() }>{text}</button>
    );

}