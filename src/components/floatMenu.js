import React, { useState } from 'react';
import { navigate } from 'gatsby';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import { FaBars } from 'react-icons/fa';
import './styles/floatMenu.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { motion } from "framer-motion"

export default () => {

    const [color, setColor] = useState('#443579');
    const [active, setActive] = useState(false);

    return (
        <DropdownButton 
            id="dropdown-basic-button" 
            title={<FaBars color={color} size={25} className="icon" />} 
            onMouseOver={ () => setColor('#fff')} 
            onMouseLeave={ () => setColor('#443579')}
        >

            <motion.div
                //className="block"
                // onClick={() => setIsActive(!isActive)}
                initial={{ scale: 0 }}
                animate={{ rotate: 0, scale: 1 }}
                transition={{
                    type: "spring",
                    stiffness: 260,
                    damping: 20
                }}
            >
                <Dropdown.Item id="item" onClick={() => navigate('/')} >Proyecto</Dropdown.Item>
                <Dropdown.Item id="item" onClick={() => navigate('/ubicacion-servicios')} >Ubicación</Dropdown.Item>
                <Dropdown.Item id="item" onClick={() => navigate('/master-plan')} >Master Plan</Dropdown.Item>
                <Dropdown.Item id="item" onClick={() => navigate('/modelos-metros')} >Modelos</Dropdown.Item>
                <Dropdown.Item id="item" onClick={() => navigate('/cocinas-360')} >Terminaciones</Dropdown.Item>
            </motion.div>

            
        </DropdownButton>
    );

}