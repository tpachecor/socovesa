import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Button from './buttonModel';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../components/styles/models.css';

export default ({ text, plain, onPress }) => {

    return (
        (!!plain) ? (
            <Col md={2} lg={2} className="ml-4 mr-4" id="square" onClick={ () => onPress() }>
                <Row className="justify-content-md-center">
                    <Col md={12} lg={12} id="cnt">
                        <Row className="justify-content-md-center" id="cntText">
                            <h1 id="meters">{text}</h1>
                            <h5 id="m"> M²</h5>
                        </Row>
                    </Col>
                </Row>
                <Row className="justify-content-md-center mt-5">
                    <Button text="Ver Más" onClick={ () => onPress() }/>
                </Row>
            </Col>
        ) : (
            <Col md={2} lg={2} className="ml-4 mr-4" id="square2" onClick={ () => onPress() }>
                <Row>
                    <Col md={6} lg={6} id="left" ></Col>
                    <Col md={6} lg={6} id="right"></Col>
                    <Col md={12} lg={12} id="cntTitle">
                        <Row className="justify-content-md-center">
                            <h1>{text}</h1>
                        </Row>
                    </Col>
                </Row>
                <Row className="justify-content-md-center">
                    <Button text="Ver Más" onClick={ () => onPress() }/>
                </Row>
            </Col>
        )
    );

}