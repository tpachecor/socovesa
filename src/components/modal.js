import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/modal.css';

export default ({ top, left, text, img }) => {

    return (
        <Container className="containerTooltip" style={{ top: `${top-13}%`, left: `${left+0.5}%` }}>
            <Row className="containerImageTooltip">
                <Image src={img} alt="" width="100%" height="auto" className="imageTooltip" fluid />
            </Row>
            <Row className="containerTooltipText">
                <p class="textTooltip"> {text} </p>
            </Row>
        </Container>
    );

}