import React from 'react';
import { graphql, StaticQuery } from 'gatsby';
import Img from 'gatsby-image';

export default ({ img, alt, className }) => {
    console.log(img)
    
    return (
        <StaticQuery 
            query={graphql`
                query {
                    images: allFile {
                        edges {
                            node {
                                relativePath
                                childImageSharp {
                                    fluid {
                                        ...GatsbyImageSharpFluid
                                    }
                                }
                            }
                        }
                    }
                }
            `}
            render={ (data) => {
                const image = data.images.edges.find( image => {
                    console.log(image);
                    return image.node.relativePath.includes(img)
                })
                return <Img alt={alt} fluid={image.node.childImageSharp.fluid} className={ (className) ? className : ''} />
            }}
        />
    );

}