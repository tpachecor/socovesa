import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { FaChevronLeft } from 'react-icons/fa';
import './styles/backButton.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export default ({ onClick }) => {

    const [color, setColor] = useState('#443579');

    return (
        <Button className="backButton" variant="light" onClick={ () => onClick() } onMouseOver={ () => setColor('#fff')} onMouseLeave={ () => setColor('#443579')}> 
            <FaChevronLeft color={color} size={25} className="icon" />
        </Button>
    );

}