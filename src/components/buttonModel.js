import React from 'react';
import './styles/buttonModel.css';

export default ({text, onClick, color = ''}) => {

    return (
        <button className="btnModel" onClick={ () => onClick() } style={{backgroundColor: color, color: (color === '') ?  '' : '#fff'}}>{text}</button>
    );

}