import React, { useState } from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/modelosCasas.css';

export default ({ img, onClick, modelo }) => {

    const [color, setColor] = useState('#E5E5E7');
    const [colorText, setColorText] = useState('#000');

    return (
        <Col md={3} lg={3}>
            <Row className="justify-content-md-center">
                <Col md={4} lg={4} className="label" style={{backgroundColor: color}}>
                    <p style={{textAlign: 'center', color: colorText, fontFamily: 'Medium', marginTop: '3px' }}>{modelo}</p>
                </Col>
            </Row>
            <Row className="justify-content-md-center" id="imgModeloCasa" onMouseOver={ () => { setColor('#443579'); setColorText('#fff') }} onMouseLeave={ () => { setColor('#E5E5E7'); setColorText('#000')  }}>
                <Image src={img} fluid id="imgModeloCasa" onMouseOver={ () => { setColor('#443579'); setColorText('#fff') }} onMouseLeave={ () => { setColor('#E5E5E7'); setColorText('#000')  }}/>
                <button 
                    className="btnModeloCasa" 
                    onClick={ () => onClick() } 
                    style={{backgroundColor: color, color: colorText}}
                    onMouseOver={ () => { setColor('#443579'); setColorText('#fff') }} onMouseLeave={ () => { setColor('#E5E5E7'); setColorText('#000')  }}
                >Ver más</button>
            </Row>
        </Col>
    );

}