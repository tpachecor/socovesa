import React from 'react';
import './styles/bullet.css';

export default ({top, left, onClick, color}) => {

    return (
        <div className="bullet" style={{ top: `${top}%`, left: `${left}%`, backgroundColor: `${color}`, transform: (color === '#443579') ? 'scale(1.5)': '' }} onClick={ () => onClick() }></div>
    ); 

}