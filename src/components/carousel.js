import React, { useEffect, useRef } from 'react';
import Glider from 'glider-js';
import './../../node_modules/glider-js/glider.min.css'
import './styles/carousel.css';

export default ({ imgs }) => {

	const carouselList = useRef();
	const carouselIndicators = useRef();

	useEffect(() => {
		new Glider(carouselList.current, {
			slidesToShow: 1,
			dots: carouselIndicators.current,
			draggable: false,
		});
	}, [])

	return (
		<div className="carousel">
			<div className="carousel-container" >
				<div className="carousel-list" ref={carouselList}>
				{
						imgs.map(item => (
							<div className="carousel-element">
								<figure>
									<img src={item.img} alt="carousel-item" className="imgCarousel" />
									<figcaption></figcaption>
								</figure>
							</div>
						))
					}
				</div>
			</div>
			<div role="tablist" className="carousel-indicators" ref={carouselIndicators}> </div>
		</div>
	);

}