import React from 'react';
import { Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/containerTitle.css';

export default ({ title, subTitle, md, lg }) => {

    return (
        <Row className="justify-content-md-center">
            <Col md={md} lg={lg} className="containerTitle">
                <h1 className="text-center">{title}</h1>
                <h5 className="text-center">{subTitle}</h5>
            </Col>
        </Row>
    );

}