import React from 'react';
import { Col, Card } from 'react-bootstrap';
import './../components/styles/card.css';
import { motion } from "framer-motion"


export default ({ md, lg, img, title, subTitle, onClick }) => {

    return (
        
        <Col md={md} lg={lg} >
            <Card id="card" onClick={() => onClick()}>
                <motion.div
                    //className="block"
                    // onClick={() => setIsActive(!isActive)}
                    animate={{
                        scale: [0.99, 1,0.99],
                    }}
                    transition={{ loop:Infinity,delay:0.3 }}
                >
                    <Card.Img variant="top" src={img} />
                </motion.div>
                <Card.Body>
                    <div id="cntHeader">
                        <Card.Title className="text-center" id="title">{title}</Card.Title>
                    </div>
                    <Card.Text className="text-center" id="subTitle">{subTitle}</Card.Text>
                </Card.Body>
            </Card>
        </Col>
        
    );

}