import React from 'react';
import { Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default ({ text }) => {

    return (
        <Row className="justify-content-md-center">
            <Col md={3} lg={3} className="containerSubTitle" style={{backgroundColor: '#443579', paddingTop: '1%', paddingBottom: '1%'}}>
                <h1 className="text-center" id="subTitle" style={{color: '#fff', fontFamily: 'Medium'}}>{text}</h1>
            </Col>
        </Row>
    );

}